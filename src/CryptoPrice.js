import React from 'react';
import axios from 'axios';

class CryptoPrice extends React.Component {
  constructor(props) {
    super(props);
    this.updateTicker = this.updateTicker.bind(this);
    this.getPrice = this.getPrice.bind(this);

    this.state = {
      ticker: '',
      price: ''
    };
  }

  updateTicker() {
    this.setState({ price: '' });
    const ticker = this.textInput.value;
    this.setState({ ticker });
  }

  getPrice(e) {
    e.preventDefault();
    this.setState({ price: '' });
    axios.get(`https://min-api.cryptocompare.com/data/price?fsym=${ this.state.ticker }&tsyms=USD`)
    .then(res => {
      const price = res.data.USD;
      this.setState({ price });
    });
  }

  render() {
    let textForCrypto = this.state.price === ''
      ? '' : `The price for ${ this.state.ticker } is currently $${ this.state.price }`;
    let invalidTicker = this.state.price === undefined || (this.state.ticker.includes('&') && this.state.price !== '')
      ? 'Sorry, this is not a valid ticker' : textForCrypto;
    return (
      <div>
        <h1>Enter a cryptocurrency ticker to display the current price!</h1>
        <form
          onSubmit={ this.getPrice }
        >
          <input
            placeholder="Enter ticker"
            type="text"
            ref={(input) => { this.textInput = input; }}
            onChange={ this.updateTicker }
          />
          <input
            type="submit"
            value="Submit"
          />
        </form>
        <br />
        <div>{ invalidTicker }</div>
      </div>
    );
  }
}

export default CryptoPrice;
